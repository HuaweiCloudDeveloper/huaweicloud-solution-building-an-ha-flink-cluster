#!/bin/bash
sleep 60s
# 获取slave-ecs的ip
IFS=',' read -ra parts <<< "$2"
# 创建一个新的空数组，用于收集分发的slave-ip数组
ip_arraylist=()
for part in "${parts[@]}"; do
    ip_arraylist+=("$part")
done

master_ip=$(ifconfig | grep 'inet ' | awk '{print $2}' | cut -d '/' -f 1 | grep -v '127.0.0.1')
servers_ip=("${master_ip}" "${ip_arraylist[@]}")
for each_ip in "${servers_ip[@]}"; do
    echo "start check $each_ip ..."
    while true; do
        status=$(curl -kv $each_ip:22)
        echo "check status $each_ip:$status ..."
        if [[ $status =~ SSH ]]; then
            echo "check ok! quit..."
            break
        else
            echo "wait 10s..."
            sleep 10s
        fi
    done
done
sleep 1s

# 安装expect nc
mkdir -p /usr/local/src && cd /usr/local/src
wget https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/building-an-ha-flink-cluster/open-source-software/tcl8.4.19-src.tar.gz
tar -zxvf tcl8.4.19-src.tar.gz
cd /usr/local/src/tcl8.4.19/unix/
./configure
make && make install
sleep 1s
cd /usr/local/src
wget https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/building-an-ha-flink-cluster/open-source-software/expect5.45.tar.gz
tar -zxvf expect5.45.tar.gz
cd /usr/local/src/expect5.45/
./configure --with-tclinclude=/usr/local/src/tcl8.4.19/generic/ --with-tclconfig=/usr/local/lib/
make && make install
sleep 1s
cd /usr/local/src
wget https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/building-an-ha-flink-cluster/open-source-software/ncat-7.94-1.x86_64.rpm
rpm -ivh ncat-7.94-1.x86_64.rpm
sleep 1s

# 配置免密登录
mkdir -p /root/.ssh
ssh-keygen -t rsa -P '' -f /root/.ssh/id_rsa
for each_ip in "${servers_ip[@]}";do
/usr/local/src/expect5.45/expect <<EOF
spawn ssh-copy-id -i /root/.ssh/id_rsa.pub root@${each_ip}
expect {
"(yes/no)?" {
send "yes\r"
expect "password:"
send "$1\r";
exp_continue
}
"password:" {
send "$1\r"
}
}
expect eof
exit
EOF
sleep 1s
done
cat /root/.ssh/id_rsa.pub >/root/.ssh/authorized_keys  #将公钥添加到authorized_keys文件中，以实现SSH无密码登录。

# 安装java环境 官网下载地址：https://download.oracle.com/java/21/archive/jdk-21.0.1_linux-x64_bin.tar.gz
mkdir -p /usr/local && cd /usr/local
wget https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/building-an-ha-flink-cluster/open-source-software/jdk-21.0.1_linux-x64_bin.tar.gz
tar -zxvf jdk-21.0.1_linux-x64_bin.tar.gz -C /usr/local
rm -f jdk-21.0.1_linux-x64_bin.tar.gz
mv /usr/local/jdk-21.0.1/ /usr/local/java
chown -R root.root /usr/local/java

# 下载flink安装包，官网下载地址：https://flink.apache.org/zh/downloads.html，环境配置centos7 以上
wget https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/building-an-ha-flink-cluster/open-source-software/flink-1.18.0-bin-scala_2.12.tgz
tar -zxvf flink-1.18.0-bin-scala_2.12.tgz -C /usr/local
 rm -f flink-1.18.0-bin-scala_2.12.tgz
mv /usr/local/flink-1.18.0 /usr/local/flink
chown -R root.root /usr/local/flink

# 配置环境变量
touch /etc/profile.d/my_env.sh
chmod 777 /etc/profile.d/my_env.sh
cat>>/etc/profile.d/my_env.sh<<EOF
export JAVA_HOME=/usr/local/java
export PATH=\$PATH:/usr/local/java/bin
export FLINK_HOME=/usr/local/flink
export PATH=\$PATH:/usr/local/flink/bin
EOF
source /etc/profile

# 系统中禁用IPv6的内核参数设置
cat>>/etc/sysctl.conf<<EOF
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1
EOF
# 使用如下命令刷新配置，使其立即生效。
/sbin/sysctl -p
/sbin/sysctl -w net.ipv4.route.flush=1

# 修改flink的配置文件
sed -i "s/jobmanager.rpc.address: localhost/jobmanager.rpc.address: $master_ip/g" /usr/local/flink/conf/flink-conf.yaml
sed -i "s/jobmanager.bind-host: localhost/jobmanager.bind-host: 0.0.0.0/g" /usr/local/flink/conf/flink-conf.yaml
sed -i "s/rest.address: localhost/rest.address: $master_ip/g" /usr/local/flink/conf/flink-conf.yaml
sed -i "s/rest.bind-address: localhost/rest.bind-address: 0.0.0.0/g" /usr/local/flink/conf/flink-conf.yaml
# masters
cat>/usr/local/flink/conf/masters<<EOF
${master_ip}:8081
EOF

# workers，遍历servers_ip数组中的每个元素
for part in "${servers_ip[@]}"; do
    if [ "${part}" == "${master_ip}" ]; then
        echo "${part}" >/usr/local/flink/conf/workers
    else
        echo "${part}" >>/usr/local/flink/conf/workers
    fi
done

# 分发flink和java的安装环境
for each_ip in "${ip_arraylist[@]}";do
   scp -r /usr/local/flink root@"${each_ip}":/usr/local
   scp -r /usr/local/java root@"${each_ip}":/usr/local
done

start-cluster.sh  # 启动集群的入口脚本

#!/bin/bash
touch /etc/profile.d/my_env.sh
chmod 777 /etc/profile.d/my_env.sh
cat>>/etc/profile.d/my_env.sh<<EOF
export JAVA_HOME=/usr/local/java
export PATH=\$PATH:/usr/local/java/bin
export FLINK_HOME=/usr/local/flink
export PATH=\$PATH:/usr/local/flink/bin
EOF
source /etc/profile

cat>>/etc/sysctl.conf<<EOF
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1
EOF
# 使用如下命令刷新配置，使其立即生效。
/sbin/sysctl -p
/sbin/sysctl -w net.ipv4.route.flush=1

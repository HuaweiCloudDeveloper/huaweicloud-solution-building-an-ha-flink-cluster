[TOC]

**解决方案介绍**
===============
该解决方案基于开源框架[Flink](https://github.com/apache/flink)，帮助您在华为云上快速构建Flink集群环境。Apache Flink是一个开源的流式数据流执行引擎，用于分布式计算，用于对无界数据流和有界数据流进行有状态计算，它可以用于对数据实时处理、批处理和流批一体化处理。如金融交易数据处理、网络流量监控、服务器日志数据分析等场景。

解决方案实践详情页面地址：
https://www.huaweicloud.com/solution/implementations/building-an-ha-flink-cluster.html

**架构图**
---------------
![方案架构](document/building-an-ha-flink-cluster.png)

**架构描述**
---------------
该解决方案会部署如下资源：

- 创建三台[弹性云服务器 ECS](https://support.huaweicloud.com/ecs/index.html)，一台作为Flink集群的JobManager角色和TaskManager角色；另外两台分别作为Flink集群的TaskManager角色。
- 创建[虚拟私有云 VPC](https://support.huaweicloud.com/vpc/index.html)、子网及安全组，保护弹性云服务器的网络安全，通过配置安全组规则，限定云服务器的访问端口，保证个人网站安全。

**组织结构**
---------------
``` lua
huaweicloud-solution-building-an-ha-flink-cluster
├── building-an-ha-flink-cluster.tf.json -- 资源编排模板
├── userdata
    ├── flink-cluster-master.sh -- JobManager脚本配置文件
    ├── flink-cluster-slave.sh -- TaskManager脚本配置文件
```

**开始使用**
---------------
1. 创建[VPC对等连接](https://support.huaweicloud.com/usermanual-vpc/zh-cn_topic_0046655036.html)后，保证不同VPC网络互通，单击该方案堆栈详情页面的“输出”，即可查看Flink集群服务的IP地址，在浏览器访问输入："http://IP地址:8081"，即可访问Flink集群后台管理系统。

   图1 验证输出回显命令

   ![验证输出回显命令](./document/readme-image-001.png)
   
2. 如Flink集群后台管理系统如下图所示，即表示集群搭建成功。

   图2 查看集群管理后台
   
   ![查看集群管理后台](./document/readme-image-002.png)
   
3. 登录弹性云服务器 ECS控制台，选择集群ECS中以master为后缀的ECS，单击“远程登录”，选择VNC方式登录。

   图3 查看弹性云服务器控制台
   
   ![查看弹性云服务器控制台](./document/readme-image-003.png)
   
   图4 VNC方式登录ECS
    
   ![VNC方式登录ECS](./document/readme-image-004.png)
   
4. 输入密码登录ECS后，进入/usr/local/flink目录，运行 flink run examples/streaming/WordCount.jar 向集群提交作业，运行正常则会显示JobId和任务运行的时间。
   
   图5 向集群提交作业
   
   ![向集群提交作业](./document/readme-image-005.png)
 
5. 在Web UI集群管理界面，点击页面的Completed Jobs，查看已经完成的WordCount任务，然后点击“WordCount”，查看对照任务的Job ID，查看提交任务的运行状态。

   图6 查看提交任务的运行状态
   
   ![查看提交任务的运行状态](./document/readme-image-006.png)
  
6. 点击下图红框中的print节点，然后依次点击“TaskManagers”->“View Taskmanager Log”，查看任务对应的标准输出结果和日志信息。

   图7 查看任务的print节点
   
   ![查看任务的print节点](./document/readme-image-007.png)
   
   图8 查看任务的标准输出结果和日志信息
   
   ![查看任务的标准输出结果和日志信息](./document/readme-image-008.png)
   